var app = angular.module('app', ['ngRoute']);

app.config(($routeProvider) => {
    $routeProvider.when('/', {
        templateUrl: './login.html',
        controller: 'loginController'
    })
        .when('/register', {
            templateUrl: './register.html',
            controller: 'registerController'
        })
        .when('/update', {
            templateUrl: './update.html',
            controller: 'updateController'
        })

        .when('/user', {
            templateUrl: './user.html',
            controller: 'adminController'

        })
        .otherwise({
            redirectTo: "/"
        });
});

app.controller('loginController', function ($scope, $http, $rootScope, $location) {
    $scope.loginSubmit = function () {
        $http.post('http://localhost:1337/login',
            {
                email: $scope.username,
                password: $scope.password
            })
            .then(function (res) {
                if (res.data == true) {
                    console.log(res)
                    alert('Usename or Password is incorrect')
                }
                else {
                    $location.path('/user')
                    console.log(res);
                    localStorage.setItem('data', JSON.stringify(res.data));


                    // $rootScope.data = res.data;

                }
            })
    }
});

app.controller('registerController', function ($scope, $http) {

    $scope.registerSubmit = function (req, res) {
        $http.post('http://localhost:1337/create',
            {
                firstname: $scope.rfirstname,
                lastname: $scope.rlastname,
                email: $scope.remail,
                password: $scope.rpassword,
                dob: $scope.rdob

            })
            .then(function (err, data) {
                if (err) throw err;
                res.json(data);
            });
    }
});
app.controller('updateController', function ($scope, $http,) {
    $scope.adminfirstname = JSON.parse(localStorage.getItem("adminfirstname"));
    $scope.adminlastname = JSON.parse(localStorage.getItem("adminlastname"));
    $scope.adminemail = JSON.parse(localStorage.getItem("adminemail"));
    $scope.admindob = JSON.parse(localStorage.getItem("admindob"));
    $scope.adminpassword = JSON.parse(localStorage.getItem("adminpassword"));
    $scope.adminid = JSON.parse(localStorage.getItem("adminid"));
    $scope.adminrole = JSON.parse(localStorage.getItem("adminrole"));
    $scope.updateSubmit = function (adminid) {
        $http.put('http://localhost:1337/update',
            {
                id: adminid,
                firstname: $scope.ufirstname,
                lastname: $scope.ulastname,
                email: $scope.uemail,
                password: $scope.upassword,
                dob: $scope.udob

            })
            .then(function (res) {

                if(res.data === true)
                {
                    alert("Admin can't update user's password");
                }
            });
    }
});

app.controller('adminController', function ($scope, $http, $rootScope, $location) {
    $scope.data = JSON.parse(localStorage.getItem("data"));
    $scope.userUpdate = function (adminemail) {
        $http.post('http://localhost:1337/adminupdate',
            {
                email: adminemail
            })
            .then(function (res) {
                $location.path('/update')
                localStorage.setItem('adminfirstname', JSON.stringify(res.data.firstname));
                localStorage.setItem('adminlastname', JSON.stringify(res.data.lastname));
                localStorage.setItem('adminemail', JSON.stringify(res.data.email));
                localStorage.setItem('admindob', JSON.stringify(res.data.dob));
                localStorage.setItem('adminpassword', JSON.stringify(res.data.password));
                localStorage.setItem('adminid', JSON.stringify(res.data.id));
                localStorage.setItem('adminrole', JSON.stringify(res.data.role));



                // $rootScope.adminfirstname = res.data.firstname;
                // $rootScope.adminlastname = res.data.lastname;
                // $rootScope.adminemail = res.data.email;
                // $rootScope.admindob = res.data.dob;
                // $rootScope.adminpassword = res.data.password;
                // $rootScope.adminid = res.data.id;
                // $rootScope.adminrole = res.data.role;
            });
    }
    $scope.deleteSubmit = function (xid) {
        console.log(xid)
        $http.delete('http://localhost:1337/delete',
            {
                id: xid
            })
            .then(function (res) {
                console.log('user deleted')
            });
    }
    $scope.logout = function () {
        $location.path('/')
        localStorage.clear();
    }
});